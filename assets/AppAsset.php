<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'assets/css/style.css',
        // 'assets/vendor/bootstrap/css/bootstrap.min.css',
        // 'assets/vendor/venobox/venobox.css',
        // 'assets/vendor/font-awesome/css/font-awesome.min.css',
    ];
    public $js = [
        'assets/js/main.js',
        // 'assets/vendor/jquery/jquery.min.js',
        // 'assets/vendor/bootstrap/js/bootstrap.bundle.min.js',
        // 'assets/vendor/jquery.easing/jquery.easing.min.js',
        // 'assets/vendor/php-email-form/validate.js',
        // 'assets/vendor/counterup/counterup.min.js',
        // 'assets/vendor/tether/js/tether.min.js',
        // 'assets/vendor/jquery-sticky/jquery.sticky.js',
        // 'assets/vendor/venobox/venobox.min.js',
        // 'assets/vendor/lockfixed/jquery.lockfixed.min.js',
        // 'assets/vendor/waypoints/jquery.waypoints.min.js',
        // 'assets/vendor/superfish/superfish.min.js',
        // 'assets/vendor/hoverIntent/hoverIntent.js',
    ];
    public $depends = [
        // 'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
    ];
}
