<?php

use yii\bootstrap\Html;

if(\Yii::$app->language == 'ru'):
    echo Html::a('Go to English', array_merge(
        \Yii::$app->request->get(),
        [\Yii::$app->homeUrl, 'language' => 'en']
    ));
    echo Html::a('O\'zbek tiliga o\'tish', array_merge(
        \Yii::$app->request->get(),
        [\Yii::$app->homeUrl, 'language' => 'uz']
    ));
else:
	if(\Yii::$app->language == 'uz'):
    echo Html::a('Go to English', array_merge(
        \Yii::$app->request->get(),
        [\Yii::$app->homeUrl, 'language' => 'en']
    ));    
    echo Html::a('Перейти на русский', array_merge(
        \Yii::$app->request->get(),
        [\Yii::$app->homeUrl, 'language' => 'ru']
    ));
    else:
		if(\Yii::$app->language == 'en'):
		    echo Html::a('O\'zbek tiliga o\'tish', array_merge(
		        \Yii::$app->request->get(),
		        [\Yii::$app->homeUrl, 'language' => 'uz']
		    ));   
		    echo Html::a('Перейти на русский', array_merge(
		        \Yii::$app->request->get(),
		        [\Yii::$app->homeUrl, 'language' => 'ru']
		    ));
		endif;
	endif;
endif;