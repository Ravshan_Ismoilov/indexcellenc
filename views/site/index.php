<style type="text/css">
	.center-text{
		padding-top: 15%; 
	}
	.xira{
		opacity: 0.4;
	}
	.tagline{
		color: black;
	}
	#logo button{
		background-color: transparent;
		border: transparent;
		color: white;
		text-transform: uppercase;
	}
	#logo ul {
		text-align: center;
		min-width: 50px;
	}
	#logo ul li{
		color: white;						
	}
</style>
	<div class="container-fluid text-center" style="padding: 0;">
		<div id="carouselExampleControls1" class="carousel slide pointer-event" data-ride="carousel">
			<div class="carousel-inner">
				<div class="carousel-item">
					<img class="d-block w-100 xira" src="assets/img/carousel/002.jpg" alt="First slide">
				</div>
				<div class="carousel-item active">
	            	<img class="d-block w-100 xira" src="assets/img/carousel/003.jpg" alt="Second slide">
				</div>
				<div class="carousel-item">
	                <img class="d-block w-100 xira" src="assets/img/carousel/005.jpg" alt="Third slide">
				</div>

				<div class="col-md-12 center-text">
	        		<h1 style="text-transform: uppercase; color: #199EB8; ">
	          			industrial excellence
	        		</h1>

	        		<p class="tagline">
	          			<?= Yii::t('common', 'Biznesning ko\'zgusi va ishonchli hamkor') ?>
	        		</p>
	        		<a class="btn scrollto" href="#about">
	        			<i class="fa fa-arrow-down fa-2x"></i>
	        			<!-- <?= Yii::t('common', 'Batafsil...') ?> -->
	        				
	        			</a>
	      		</div>
			</div>
	        <a class="carousel-control-prev" href="#carouselExampleControls1" role="button" data-slide="prev">
	        	<span class="carousel-control-prev-icon" aria-hidden="true"></span>
	            <span class="sr-only"> <?= Yii::t('common', 'Previous') ?></span>
			</a>
			<a class="carousel-control-next" href="#carouselExampleControls1" role="button" data-slide="next">
	        	<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only"> <?= Yii::t('common', 'Next') ?></span>
			</a>
		</div>
	</div>

  	<!-- ======= Header ======= -->
  	<header id="header">
    	<div class="container">
			<div id="logo" class="pull-left">
        		<a href="#header" class="scrollto">
        			<img src="assets/img/logo-nav.png" alt="">
        		</a>
				<!-- <img src="assets/img/uz.png" width="30px" height="30px" style="border: 1px solid white;border-radius: 50%;"> -->

				  	<button type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				  		<?= \Yii::$app->language; ?>
				  	</button>
				  	<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
				    	<li> <?php echo yii\helpers\Html::a('Uz',array_merge(\Yii::$app->request->get(),[\Yii::$app->homeUrl,'language'=>'uz'])); ?> </li>
				    	<li> <?php echo yii\helpers\Html::a('Ru',array_merge(\Yii::$app->request->get(),[\Yii::$app->homeUrl,'language'=>'ru'])); ?> </li>
				    	<li> <?php echo yii\helpers\Html::a('En',array_merge(\Yii::$app->request->get(),[\Yii::$app->homeUrl,'language'=>'en'])); ?> </li>
				  	</ul>
      		</div>
			<nav id="nav-menu-container">
        		<ul class="nav-menu">
					<li><a href="#header"><?= Yii::t('common', 'Bosh sahifa') ?></a></li>
          			<li><a href="#about"><?= Yii::t('common', 'Biz haqimizda') ?></a></li>
          			<li><a href="#features"><?= Yii::t('common', 'Asosiy faoliyat') ?></a></li>
          			<li><a href="#team"><?= Yii::t('common', 'Vacancy') ?></a></li>
          			<li><a href="#contact"><?= Yii::t('common', 'Biz bilan bog\'lanish') ?></a></li>
        		</ul>
      		</nav>
      		<!-- #nav-menu-container -->
			<nav class="nav social-nav pull-right d-none d-lg-inline">
        		<a href="#"><i class="fa fa-twitter"></i></a>
        		<a href="#"><i class="fa fa-facebook"></i></a>
        		<a href="#"><i class="fa fa-linkedin"></i></a>
        		<a href="#"><i class="fa fa-envelope"></i></a>        		
			</nav>
    	</div>
  	</header>
  	<!-- End Header -->

  	<main id="main">
	    <!-- ======= Faoliyat Section ======= -->
    	<section class="features" id="features">
      		<div class="container">
        		<h2 class="text-center"> <?= Yii::t('common', 'Faoliyat') ?> </h2>
	        	<div class="row">
	          		<div class="feature-col col-lg-4 col-xs-12">
	            		<div class="card card-block text-center" data-toggle="modal" data-target="#myModal" style="cursor: pointer;">
							<div>
							    <div class="feature-icon">
							    	<span class="fa fa-rocket"></span>
								</div>
							</div>
							<div >
								<h3> <?= Yii::t('common', 'Biznesni rivojlantirish va operatsion boshqaruvda consulting xizmatlari') ?> </h3>
							</div>
	            		</div>
	          		</div>
	          			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
						    	<div class="modal-content">
						      		<div class="modal-header">
						        		<h4 class="modal-title" id="myModalLabel">
						        			<?= Yii::t('common', 'Biznesdagi mavjud muammolar va imkoniyatlarni aniqlash') ?>
						       			</h4>
						        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						      		</div>
						      		<div class="modal-body">
					      				<blockquote class="text-left">
					      					<p><?= Yii::t('common', 'Mahsulot sifatini oshirish') ?></p>
					      				</blockquote>
					      				<hr>
					      				<blockquote class="text-left">
					      					<p><?= Yii::t('common', 'Qisqa va uzoq muddatli rivojlantirish strategiyalarini ishlab chiqish') ?></p>
					      				</blockquote>
						      		</div>
						    	</div>
						  	</div>
						</div>
	          		<div class="feature-col col-lg-4 col-xs-12" >
	            		<div class="card card-block text-center" data-toggle="modal" data-target="#myModal1" style="cursor: pointer;">
	              			<div>
	                			<div class="feature-icon">
	                  				<span class="fa fa-dashboard"></span>
	                			</div>
	              			</div>
	              			<div>
								<h3><?= Yii::t('common', 'Innovatsiya va Texnologiyalar transferi')?></h3>
							</div>
	            		</div>
	          		</div>
	          			<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
						    	<div class="modal-content">
						      		<div class="modal-header">
						        		<h4 class="modal-title" id="myModalLabel">
						        			<?= Yii::t('common', 'Biznesni uzluksiz rivojlantirishda takliflar portfelini yaratish') ?>
						       			</h4>
						        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						      		</div>
						      		<div class="modal-body">
					      				<blockquote class="text-left">
					      					<p><?= Yii::t('common', 'Operatsion boshqaruv tizimini takomillashtirish') ?></p>
					      				</blockquote>
					      				<hr>
					      				<blockquote class="text-left">
					      					<p><?= Yii::t('common', 'Biznes maqsadlariga yo\'naltirilgan asosiy samaradorlik ko\'rsatkichlarini ishlab chiqish') ?></p>
					      				</blockquote>
						      		</div>
						    	</div>
						  	</div>
						</div>
	          		<div class="feature-col col-lg-4 col-xs-12">
	            		<div class="card card-block text-center" data-toggle="modal" data-target="#myModal2" style="cursor: pointer;">
	              			<div>
	                			<div class="feature-icon">
	                  				<span class="fa  fa-envelope"></span>
	                			</div>
	              			</div>
	              			<div>
								<h3><?= Yii::t('common', 'Ilm, fan va tadqiqot') ?></h3>
							</div>
	            		</div>
	          		</div>
	          			<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
						    	<div class="modal-content">
						      		<div class="modal-header">
						        		<h4 class="modal-title" id="myModalLabel">
						        			<?= Yii::t('common', 'LEAN & 6 - SIGMA texnologiyalarini joriy etish') ?>
						       			</h4>
						        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						      		</div>
						      		<div class="modal-body">
					      				<blockquote class="text-left">
					      					<p><?= Yii::t('common', 'Jarayonni optimallashtirish') ?></p>
					      				</blockquote>
					      				<hr>
					      				<blockquote class="text-left">
					      					<p><?= Yii::t('common', 'Imkoniyatlarni aniqlash va uzluksiz rivojlantirish tavsiyalarini ishlab chiqish') ?></p>
					      				</blockquote>
						      		</div>
						    	</div>
						  	</div>
						</div>
	        	</div>
	      	</div>
		</section>
		<!-- End Features Section -->
    	
    	<!-- ======= Call to Action Section ======= -->
	    <section class="cta">
	      	<div class="container">
	        	<div class="row">
	          		<div class="col-lg-12 col-sm-12 text-center">
	            		<h2 style="text-transform: uppercase">industrial excellence</h2>
	            		<p class="tagline mt-2">
          					<?= Yii::t('common', 'Biznesning ko\'zgusi va sizning ishonchli hamkoringiz. Biz 10 yildan ortiq ishlab chiqarish tajribasiga egamiz') ?>
        				</p>
	          		</div>
	        	</div>
	      	</div>
	    </section>
	    <!-- End Call to Action Section -->

    	<!-- ======= About Section ======= -->
    	<section class="about" id="about">
			<div class="container text-center">
        		<h2>INDUSTRIAL EXCELLENCE</h2>
		        <p><?= Yii::t('common', 'Biz ishlab chiqarish jarayonida quyidagi xizmatlarni taklif etamiz va qo\'llab quvvatlaymiz') ?></p>
				<div class="row stats-row">
          			<div class="stats-col text-center col-md-4 col-sm-6">
            			<div class="circle" style="width: 200px; height: 200px;">
              				<!-- <span class="stats-no" data-toggle="counter-up">232</span> -->
              				<span class="stats-no"> <i class="fa fa-check fa-2x" aria-hidden="true"></i> </span> <?= Yii::t('common', 'Biznesdagi mavjud muammolar va imkoniyatlarni aniqlash') ?>
            			</div>
						<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
						    	<div class="modal-content">
						      		<div class="modal-header">
						        		<h4 class="modal-title" id="myModalLabel">
						        			<?= Yii::t('common', 'Biznesdagi mavjud muammolar va imkoniyatlarni aniqlash') ?>
						       			</h4>
						        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						      		</div>
						      		<div class="modal-body">
					      				<blockquote class="text-left">
					      					<p><?= Yii::t('common', 'Mahsulot sifatini oshirish') ?></p>
					      				</blockquote>
					      				<hr>
					      				<blockquote class="text-left">
					      					<p><?= Yii::t('common', 'Qisqa va uzoq muddatli rivojlantirish strategiyalarini ishlab chiqish') ?></p>
					      				</blockquote>
						      		</div>
						    	</div>
						  	</div>
						</div>
          			</div>

          			<div class="stats-col text-center col-md-4 col-sm-6">
            			<div class="circle" style="width: 200px; height: 200px;">
              				<!-- <span class="stats-no" data-toggle="counter-up">232</span> -->
              				<span class="stats-no"> <i class="fa fa-check fa-2x" aria-hidden="true"></i> </span> <?= Yii::t('common', 'Biznesni uzluksiz rivojlantirishda takliflar portfelini yaratish') ?>
            			</div>
						<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
						    	<div class="modal-content">
						      		<div class="modal-header">
						        		<h4 class="modal-title" id="myModalLabel">
						        			<?= Yii::t('common', 'Biznesni uzluksiz rivojlantirishda takliflar portfelini yaratish') ?>
						       			</h4>
						        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						      		</div>
						      		<div class="modal-body">
					      				<blockquote class="text-left">
					      					<p><?= Yii::t('common', 'Operatsion boshqaruv tizimini takomillashtirish') ?></p>
					      				</blockquote>
					      				<hr>
					      				<blockquote class="text-left">
					      					<p><?= Yii::t('common', 'Biznes maqsadlariga yo\'naltirilgan asosiy samaradorlik ko\'rsatkichlarini ishlab chiqish') ?></p>
					      				</blockquote>
						      		</div>
						    	</div>
						  	</div>
						</div>
          			</div>
					
					<div class="stats-col text-center col-md-4 col-sm-6" style="margin-top: 20px;">
            			<div class="circle" style="width: 200px; height: 200px;">
              				<!-- <span class="stats-no" data-toggle="counter-up">232</span> -->
              				<span class="stats-no"> <i class="fa fa-check fa-2x" aria-hidden="true"></i> </span> <?= Yii::t('common', 'LEAN & 6 - SIGMA texnologiyalarini joriy etish') ?>
            			</div>
						<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
						    	<div class="modal-content">
						      		<div class="modal-header">
						        		<h4 class="modal-title" id="myModalLabel">
						        			<?= Yii::t('common', 'LEAN & 6 - SIGMA texnologiyalarini joriy etish') ?>
						       			</h4>
						        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						      		</div>
						      		<div class="modal-body">
					      				<blockquote class="text-left">
					      					<p><?= Yii::t('common', 'Jarayonni optimallashtirish') ?></p>
					      				</blockquote>
					      				<hr>
					      				<blockquote class="text-left">
					      					<p><?= Yii::t('common', 'Imkoniyatlarni aniqlash va uzluksiz rivojlantirish tavsiyalarini ishlab chiqish') ?></p>
					      				</blockquote>
						      		</div>
						    	</div>
						  	</div>
						</div>
          			</div>
        		</div>
      		</div>
		</section>
		<!-- End About Section -->

	    <!-- ======= Call to Action Section ======= -->
	    <section class="cta">
	      	<div class="container">
	        	<div class="row">
	          		<div class="col-lg-12 col-sm-12 text-center">
	            		<h2 class="text-center" style="text-transform: uppercase;"> <?= Yii::t('common', 'Bizning jamoa') ?></h2>
	          		</div>
	        	</div>
	      	</div>
	    </section>
	    <!-- End Call to Action Section -->
	    
	    <!-- ======= Team Section ======= -->
	    <section class="team" id="team">
	      	<div class="container">
	        	<!-- <h2 class="text-center"> <?= Yii::t('common', 'Bizning jamoa') ?></h2> -->
	        	<div class="row">
	          		<div class="col-sm-3 col-xs-6">
	            		<div class="card card-block">
	              			<a href="#"><img alt="" class="team-img" src="assets/img/team-1.jpg">
	                			<div class="card-title-wrap">
	                  				<span class="card-title"><?= Yii::t('common', 'Jahongir Samatov') ?></span> <span class="card-text"><?= Yii::t('common', 'asoschi va ijrochi direktor') ?></span>
	                			</div>
	                			<div class="team-over">
	                  				<h4 class="hidden-md-down"><?= Yii::t('common', 'Men bilan aloqa') ?></h4>
	                  				<nav class="social-nav">
	                    				<a href="#"><i class="fa fa-twitter"></i></a>
	                    				<a href="#"><i class="fa fa-facebook"></i></a>
	                    				<a href="#"><i class="fa fa-linkedin"></i></a>
	                    				<a href="#"><i class="fa fa-envelope"></i></a>
	                  				</nav>
	                  				<p><?= Yii::t('common', 'Yuqoridagi ijtimoiy tarmoqlar orqali bog\'lanishingiz mumkin') ?>.</p>
	                			</div>
	              			</a>
	            		</div>
	          		</div>
	        	</div>
	      	</div>
	    </section><!-- End Team Section -->
	    <!-- ======= Contact Section ======= -->
	    <section id="contact">
	      	<div class="container">
	        	<div class="row">
	          		<div class="col-md-12 text-center">
	            		<h2 class="section-title"><?= Yii::t('common', 'Biz bilan bog\'lanish') ?></h2>
	          		</div>
	        	</div>
	        	<div class="row justify-content-center">
	          		<div class="col-lg-3 col-md-4">
	            		<div class="info">
	              			<div>
	                			<i class="fa fa-map-marker"></i>
	                			<p><?= Yii::t('common', 'O\'zbekiston Respublikasi, Namangan viloyati,<br>Namangan shahar, I.Karimov ko\'chasi 12-uy ') ?></p>
	              			</div>
	              			<div>
	                			<i class="fa fa-envelope"></i>
	                			<p>info@indexcellence.uz</p>
	              			</div>
	              			<div>
	                			<i class="fa fa-phone"></i>
	                			<p>+998 90 900 57 76</p>
	              			</div>
	            		</div>
	          		</div>
		          	<div class="col-lg-5 col-md-8">
		            	<div class="form">
		              		<form action="forms/contact.php" method="post" role="form" class="php-email-form">
		                		<div class="form-group">
		                  			<input type="text" name="name" class="form-control" id="name" placeholder="<?= Yii::t('common', 'Ismingiz') ?>" data-rule="minlen:4" data-msg="<?= Yii::t('common', 'Belgilar soni 4 tadan kam...') ?>" />
		                  			<div class="validate"></div>
		                		</div>
		                		<div class="form-group">
		                  			<input type="email" class="form-control" name="email" id="email" placeholder="<?= Yii::t('common', 'Sizning Email') ?>" data-rule="email" data-msg="<?= Yii::t('common', 'Siz noto\'g\'ri email kiritdingiz...') ?>" />
		                  			<div class="validate"></div>
		                		</div>
		                		<div class="form-group">
		                  			<input type="text" class="form-control" name="subject" id="subject" placeholder="<?= Yii::t('common', 'Mavzu') ?>" data-rule="minlen:4" data-msg="<?= Yii::t('common', '8 ta belgidan kam bo\'lmagan mavzu kiriting...') ?>" />
		                  			<div class="validate"></div>
		                		</div>
		                		<div class="form-group">
		                  			<textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="<?= Yii::t('common', 'Asosiy xabar') ?>" placeholder="<?= Yii::t('common', 'Xabar matni') ?>"></textarea>
		                  			<div class="validate"></div>
		                		</div>
		                		<div class="mb-3">
		                  			<div class="loading"> <?= Yii::t('common', 'Yuborilmoqda...') ?></div>
		                  			<div class="error-message"></div>
		                  			<div class="sent-message"> <?= Yii::t('common', 'Sizning xabaringiz muvaffaqiyatli yuborildi!') ?></div>
		                		</div>
		                		<div class="text-center"><button type="submit"> <?= Yii::t('common', 'Yuborish') ?></button></div>
		              		</form>
		            	</div>
		          	</div>
				</div>
		    </div>
	    </section><!-- End Contact Section -->
  	</main><!-- End #main -->

  	<!-- ======= Footer ======= -->
  	<footer class="site-footer">
    	<div class="bottom">
      		<div class="container">
        		<div class="row">
          			<div class="col-lg-6 col-xs-12 text-lg-left text-center">
            			<p class="copyright-text">&copy; Copyright <strong>Industrial excellence</strong>. All Rights Reserved</p>
	            		<div class="credits">Designed by <a href="https://telegram.me/ravshan_ismoilov"> Ravshan Ismoilov </a></div>
          			</div>
          			<div class="col-lg-6 col-xs-12 text-lg-right text-center">
            			<ul class="list-inline">
            				<li class="list-inline-item"><a href="#about"> <?= Yii::t('common', 'Biz haqimizda ') ?></a></li>
          					<li class="list-inline-item"><a href="#features"> <?= Yii::t('common', 'Faoliyat ') ?></a></li>
          					<!-- <li><a href="#portfolio">Portfolio</a></li> -->
          					<li  class="list-inline-item"><a href="#team"> <?= Yii::t('common', 'Bizning jamoa ') ?></a></li>
          					<li  class="list-inline-item"><a href="#contact"> <?= Yii::t('common', 'Biz bilan bog\'lanish ') ?></a></li>
            			</ul>
          			</div>
        		</div>
      		</div>
    	</div>
  	</footer>
  	<!-- End Footer -->

  	<a class="scrolltop" href="#"><span class="fa fa-angle-up"></span></a>

  	<!-- Vendor JS Files -->
  	<script src="assets/vendor/jquery/jquery.min.js"></script>
  	<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  	<script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  	<script src="assets/vendor/php-email-form/validate.js"></script>
  	<script src="assets/vendor/counterup/counterup.min.js"></script>
  	<script src="assets/vendor/tether/js/tether.min.js"></script>
  	<script src="assets/vendor/jquery-sticky/jquery.sticky.js"></script>
  	<script src="assets/vendor/venobox/venobox.min.js"></script>
  	<script src="assets/vendor/lockfixed/jquery.lockfixed.min.js"></script>
  	<script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  	<script src="assets/vendor/superfish/superfish.min.js"></script>
  	<script src="assets/vendor/hoverIntent/hoverIntent.js"></script>